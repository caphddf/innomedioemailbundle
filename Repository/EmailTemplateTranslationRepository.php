<?php

namespace Innomedio\EmailBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Innomedio\EmailBundle\Entity\EmailTemplateTranslation;

class EmailTemplateTranslationRepository extends EntityRepository
{
    /**
     * @param $tag
     * @param $languageId
     * @return EmailTemplateTranslation|null
     */
    public function getEmailTemplateForLanguageByTag($tag, $languageId)
    {
        try {
            return
                $this
                    ->createQueryBuilder('t')
                    ->leftJoin('t.emailTemplate', 'e')
                    ->where('e.tag = :tag')
                    ->andWhere('t.language = :languageId')
                    ->setParameter('tag', $tag)
                    ->setParameter('languageId', $languageId)
                    ->getQuery()
                    ->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
