<?php
namespace Innomedio\EmailBundle\Twig\Backend;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\EmailBundle\Entity\Mail;
use Twig\TwigFunction;

class LatestEmailsExtension extends \Twig_Extension
{
    private $em;

    /**
     * LatestEmailsExtension constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('latestEmails', array($this, 'latestEmails'))
        );
    }

    /**
     * @param $amount
     * @param $tag
     * @return array|Mail[]
     */
    public function latestEmails($amount, $tag = null)
    {
        if (!$tag) {
            $condition = array();
        } else {
            $condition = array('tag' => $tag);
        }

        return $this->em->getRepository('InnomedioEmailBundle:Mail')->findBy($condition, array('sent' => 'desc'), $amount);
    }
}