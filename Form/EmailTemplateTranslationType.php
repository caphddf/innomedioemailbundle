<?php

namespace Innomedio\EmailBundle\Form;

use Innomedio\BackendThemeBundle\Entity\Language;
use Innomedio\EmailBundle\Entity\EmailTemplateTranslation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmailTemplateTranslationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', EntityType::class, array(
                'class' => Language::class,
                'choice_label' => 'name',
                'label' => 'innomedio.backend_theme.label.code'
            ))
            ->add('subject', TextType::class, array(
                'label' => 'innomedio.email.subject',
                'required' => false
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'innomedio.backend_theme.global.content',
                'required' => false,
                'attr' => array('rows' => 15)
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => EmailTemplateTranslation::class,
            'allow_extra_fields' => true
        ));
    }
}