<?php

namespace Innomedio\EmailBundle\Form;

use Innomedio\EmailBundle\Entity\EmailTemplate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmailTemplateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', CollectionType::class, array(
                'allow_extra_fields' => true,
                'entry_type' => EmailTemplateTranslationType::class,
                'by_reference' => false,
                'allow_add' => true
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'innomedio.backend_theme.global.save'
            ))
        ;

        if ($options['is_root']) {
            $builder
                ->add('name', TextType::class, array(
                    'required' => true,
                    'label' => 'innomedio.backend_theme.global.name'
                ))
                ->add('tag', TextType::class, array(
                    'required' => true,
                    'label' => 'innomedio.backend_theme.global.tag'
                ))
                ->add('info', TextareaType::class, array(
                    'attr' => array('class' => 'summernote'),
                    'label' => 'innomedio.email_template.info',
                    'required' => false
                ))
            ;
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => EmailTemplate::class,
            'allow_extra_fields' => true,
            'is_root' => false
        ));
    }
}