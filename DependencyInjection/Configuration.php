<?php
namespace Innomedio\EmailBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('innomedio_email');
        $rootNode
            ->children()
                ->scalarNode('has_templates')
                    ->defaultValue(true)
                ->end()
                ->scalarNode('from_email')
                    ->isRequired()
                ->end()
                ->scalarNode('mail_type')
                    ->defaultValue("swiftmailer")
                ->end()
                ->scalarNode('postmark_api_token')
                    ->defaultValue(null)
                ->end()
            ->end();

        return $treeBuilder;
    }
}
