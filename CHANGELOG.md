# 1.0.8

* Did some code finetuning
* Moved the docs to the new location
* The e-mail object is automatically cleaned once a mail has been send
* You can now send mails using Swiftmailer or Postmark

# 1.0.7

* Entity fix for utf8mb4

# 1.0.6

* Docs changes
* Entity fix

# 1.0.5

* Added new method to EmailTemplateTranslationRepository

# 1.0.4

* Some docs changes
* Changed all template includes to lowercase dirs
* Added e-mail templates which normal users can edit. Root users can add templates
* You can now use $mailer->clean() send new mails in one request

# 1.0.3

* Some docs changes
* Attachments can be added, and will be also be archived

# 1.0.2

* Changed route file for BackendThemeBundle 1.1 change (subdomain) and the documentation for implementation.
* Removed the unnecessary setup command
* Emails that don't have a tag are not being stored anymore

# 1.0.1

* Fixed correct time format for email list

# 1.0

* Start!