<?php
namespace Innomedio\EmailBundle\Service;

use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarContainer;
use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarExtension;
use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarItem;
use Symfony\Component\Routing\Router;

class EmailSidebarExtension extends SidebarExtension
{
    private $sidebarContainer;
    private $router;
    private $hasTemplates;

    /**
     * SidebarItems constructor.
     * @param SidebarContainer $sidebarContainer
     * @param Router $router
     */
    public function __construct(SidebarContainer $sidebarContainer, Router $router, $hasTemplates)
    {
        $this->sidebarContainer = $sidebarContainer;
        $this->router = $router;
        $this->hasTemplates = $hasTemplates;
    }

    /**
     * @return array|SidebarItem
     */
    public function getSidebars()
    {
        $nav = array(
            $this->emails(),
        );

        if ($this->hasTemplates) {
            $nav[] = $this->emailTemplates();
        }

        return $nav;
    }

    /**
     * @return SidebarItem
     */
    public function emails()
    {
        $admin = $this->sidebarContainer->getMainItem('admin');

        $email = new SidebarItem();
        $email->setIcon('fa-envelope');
        $email->setName('innomedio.email.emails');
        $email->setLink($this->router->generate('innomedio.email.index'));
        $email->setTag('email');
        $email->setParent($admin);
        $email->setRole('ROLE_EMAIL');

        return $email;
    }

    /**
     * @return SidebarItem
     */
    public function emailTemplates()
    {
        $admin = $this->sidebarContainer->getMainItem('admin');

        $email = new SidebarItem();
        $email->setIcon('fa-file-o');
        $email->setName('innomedio.email.email_templates');
        $email->setLink($this->router->generate('innomedio.email.templates.list'));
        $email->setTag('email_templates');
        $email->setParent($admin);
        $email->setRole('ROLE_EMAIL');

        return $email;
    }
}