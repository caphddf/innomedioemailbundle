<?php
namespace Innomedio\EmailBundle\Service;

use Innomedio\EmailBundle\Entity\Mail;
use Doctrine\ORM\EntityManagerInterface;
use Postmark\Models\PostmarkAttachment;
use Postmark\Models\PostmarkException;
use Postmark\PostmarkClient;
use Symfony\Component\Filesystem\Filesystem;
use Zend\Code\Scanner\FileScanner;

class MailManager
{
    private $tag = null;
    private $subject;
    private $to = array();
    private $from;
    private $bcc = array();
    private $html;
    private $replyTo = null;
    private $attachments = array();

    private $em;
    private $mailer;
    private $projectDir;

    private $sendType;
    private $postmarkApiToken;

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return array
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param array $to
     */
    public function setTo($to): void
    {
        if (!is_array($to)) {
            $to = array($to);
        }

        $this->to = $to;
    }

    /**
     * @return array
     */
    public function getBcc(): array
    {
        return $this->bcc;
    }

    /**
     * @param array $bcc
     */
    public function setBcc(array $bcc): void
    {
        if (!is_array($bcc)) {
            $bcc = array($bcc);
        }

        $this->bcc = $bcc;
    }

    /**
     * @return mixed
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * @param mixed $html
     */
    public function setHtml($html): void
    {
        $this->html = $html;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param $file
     */
    public function addAttachment($file)
    {
        $this->attachments[] = $file;
    }

    /**
     * @return null
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * @param null $replyTo
     */
    public function setReplyTo($replyTo): void
    {
        $this->replyTo = $replyTo;
    }

    /**
     * MailManager constructor.
     * @param \Swift_Mailer $mailer
     * @param EntityManagerInterface $em
     * @param $projectDir
     * @param $sendType
     * @param $fromEmail
     * @param $postmarkApiKey
     */
    public function __construct(\Swift_Mailer $mailer, EntityManagerInterface $em, $projectDir, $sendType, $fromEmail, $postmarkApiToken)
    {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->projectDir = $projectDir;
        $this->from = $fromEmail;
        $this->sendType = $sendType;
        $this->postmarkApiToken = $postmarkApiToken;
    }

    /**
     *
     */
    public function clean()
    {
        $this->tag = null;
        $this->subject = null;
        $this->to = null;
        $this->bcc = null;
        $this->html = null;
        $this->attachments = array();
    }

    /**
     * @param \Swift_Mailer $mailer
     * @param EntityManagerInterface $em
     * @return bool|null
     */
    public function sendEmail()
    {
        switch ($this->sendType) {
            case "swiftmailer":
                return $this->sendSwiftmailer();
            break;

            case "postmark":
                return $this->sendPostmark();
            break;
        }
    }

    /**
     * @param $file
     * @param $key
     * @return array
     */
    public function storeAttachment($file, $key)
    {
        $pathinfo = pathinfo($file);
        $newLocation = $this->projectDir . "/var/email/" . (new \DateTime)->getTimestamp() . "-" . $key . "-" . rand(1, 1000) . "/" . $pathinfo['basename'];

        $fs = new Filesystem();
        $fs->copy($file, $newLocation);

        return array(
            'dir' => $newLocation,
            'filename' => $pathinfo['basename']
        );
    }

    /**
     * @param $errorLog
     * @param $headers
     * @param $attachmentsDump
     * @param $postmarkId
     */
    public function storeEmail($errorLog, $headers, $attachmentsDump, $postmarkId = null)
    {
        if ($this->getTag()) {
            $mail = new Mail();
            $mail->setErrors($errorLog);
            $mail->setHeaders($headers);
            $mail->setPostmarkId($postmarkId);
            $mail->setHtml($this->html);
            $mail->setTag($this->tag);
            $mail->setSubject($this->subject);
            $mail->setTo(implode(", ", $this->to));
            $mail->setAttachments(json_encode($attachmentsDump));
            $mail->setType($this->sendType);

            $this->em->persist($mail);
            $this->em->flush();
        }
    }

    /**
     * @return bool|null
     */
    public function sendSwiftmailer()
    {
        // First send the mail
        $return = null;
        $errorLog = null;

        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $this->mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

        $message = (new \Swift_Message($this->subject))
            ->setFrom($this->from)
            ->setTo($this->to)
            ->setBody($this->html,'text/html')
        ;

        if ($this->getReplyTo()) {
            $message->setReplyTo($this->getReplyTo());
        }

        $attachmentsDump = array();
        foreach ($this->attachments as $key => $file) {
            $message->attach(\Swift_Attachment::fromPath($file));

            // If the e-mail is being stored, copy the file to our var folder to keep an archive of all attachments
            if ($this->tag) {
                $attachmentsDump[] = $this->storeAttachment($file, $key);
            }
        }

        // Send e-mail
        if (!$this->mailer->send($message, $errors)) {
            $errorLog = $logger->dump();
            $return = false;
        } else {
            $return = true;
        }

        // Add e-mail to the database
        $this->storeEmail($errorLog, $message->getHeaders(), $attachmentsDump);

        // Clean so new mails can be sent
        $this->clean();

        return $return;
    }

    /**
     * @return bool
     */
    public function sendPostmark()
    {
        $errorLog = null;
        $return = false;
        $attachmentsDump = array();
        $postmarkId = null;

        try {
            $postmarkAttachments = array();

            foreach ($this->attachments as $key => $file) {
                $pathinfo = pathinfo($file);
                $postmarkAttachments[] = PostmarkAttachment::fromFile($file, $pathinfo['basename']);

                if ($this->tag) {
                    $attachmentsDump[] = $this->storeAttachment($file, $key);
                }
            }

            $client = new PostmarkClient($this->postmarkApiToken);
            $sendResult = $client->sendEmail(
                $this->from,
                implode(",", $this->getTo()),
                $this->getSubject(),
                $this->getHtml(),
                null,
                $this->getTag(),
                true,
                $this->getReplyTo(),
                null,
                implode(",", $this->getBcc()),
                null,
                $postmarkAttachments
            );

            $postmarkId = $sendResult->messageid;

            $return = true;
        } catch (PostmarkException $e) {
            $errorLog = $e->getCode() . ": " . $e->getMessage();
        } catch (\Exception $e) {
            $errorLog = $e->getCode() . ": " . $e->getMessage();
        }

        $this->storeEmail($errorLog, '', $attachmentsDump, $postmarkId);
        $this->clean();

        return $return;
    }
}