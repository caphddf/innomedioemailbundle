# InnomedioEmailBundle

[![Scrutinizer Code Quality](https://scrutinizer-ci.com/gl/developer/innomedio_internal_symfony/InnomedioEmailBundle/badges/quality-score.png?b=master&s=d705e31f5f83c29422435109f60c537a5ec5eaae)](https://scrutinizer-ci.com/gl/developer/innomedio_internal_symfony/InnomedioEmailBundle/?branch=master)
[![Build Status](https://scrutinizer-ci.com/gl/developer/innomedio_internal_symfony/InnomedioEmailBundle/badges/build.png?b=master&s=a7a52c4e5e75b238da9d96a2c8a144e72edd0852)](https://scrutinizer-ci.com/gl/developer/innomedio_internal_symfony/InnomedioEmailBundle/build-status/master)

[Documentation](http://docs.innomedio.work/)