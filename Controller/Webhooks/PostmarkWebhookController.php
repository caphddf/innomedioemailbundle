<?php
namespace Innomedio\EmailBundle\Controller\Webhooks;

use Innomedio\EmailBundle\Entity\Mail;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/webhooks/postmark")
 */
class PostmarkWebhookController extends Controller
{
    /**
     * @Route("/")
     */
    public function processEvent(Request $request)
    {
        $json = json_decode($request->getContent(), true);

        if (isset($json['MessageID']) && isset($json['RecordType'])) {
            $mail = $this->getDoctrine()->getRepository('InnomedioEmailBundle:Mail')->findOneBy(array('postmarkId' => $json['MessageID']));

            if ($mail) {
                switch ($json['RecordType']) {
                    case "Delivery":
                        return $this->addEvent(
                            $mail,
                            $json['RecordType'],
                            $json['Recipient'],
                            $json['DeliveredAt'],
                            $json['Details']
                        );
                    break;

                    case "Bounce":
                        return $this->addEvent(
                            $mail,
                            $json['RecordType'],
                            $json['Email'],
                            $json['BouncedAt'],
                            $json['Description']
                        );
                    break;

                    case "Open":
                        if (isset($json['FirstOpen']) && $json['FirstOpen'] === true) {
                            return $this->addEvent(
                                $mail,
                                $json['RecordType'],
                                $json['Recipient'],
                                null,
                                ''
                            );
                        }
                    break;

                    case "SpamComplaint":
                        return $this->addEvent(
                            $mail,
                            $json['RecordType'],
                            $json['Email'],
                            $json['BouncedAt'],
                            $json['Description'] . " / " . $json['Details']
                        );
                    break;
                }
            }
        }

        //return $this->render('base.html.twig');

        return new JsonResponse(array('success' => false));
    }

    /**
     * @param Mail $mail
     * @param $type
     * @param $email
     * @param $date
     * @param $info
     * @return JsonResponse
     */
    private function addEvent(Mail $mail, $type, $email, $date, $info)
    {
        $em = $this->getDoctrine()->getManager();

        if ($date === null) {
            $date = new \DateTime("now");
        } else {
            $date = new \DateTime($date);
        }

        $currentData = json_decode($mail->getPostmarkInfo());
        $currentData[] = array(
            'type' => $type,
            'email' => $email,
            'date' => $date->format('d/m/Y H:i:s'),
            'info' => $info
        );

        $mail->setPostmarkInfo(json_encode($currentData));
        $em->persist($mail);
        $em->flush();

        return new JsonResponse(array('success' => true));
    }
}

