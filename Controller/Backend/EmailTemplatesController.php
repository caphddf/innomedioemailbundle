<?php
namespace Innomedio\EmailBundle\Controller\Backend;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\BackendThemeBundle\Service\Ajax\AjaxResponse;
use Innomedio\EmailBundle\Entity\EmailTemplate;
use Innomedio\EmailBundle\Form\EmailTemplateType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("has_role('ROLE_EMAIL')")
 * @Route("/email/templates")
 */
class EmailTemplatesController extends BackendThemeController
{
    /**
     * @Route("/", name="innomedio.email.templates.list")
     * @return Response
     */
    public function list()
    {
        $this->check();

        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.email.email_templates');

        return $this->render('@InnomedioEmail/backend/templates/list.html.twig', array(
            'templates' => $this->getDoctrine()->getRepository('InnomedioEmailBundle:EmailTemplate')->findAll()
        ));
    }

    /**
     * @Route(
     *     "/form/{id}",
     *     name="innomedio.email.templates.form"),
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     * )
     *
     * @param EmailTemplate|null $emailTemplate
     * @return Response
     */
    public function form(EmailTemplate $emailTemplate = null)
    {
        $this->check();

        $action = "edit";

        if (!$emailTemplate) {
            $emailTemplate = new EmailTemplate();
            $action = 'add';

            if (!$this->isGranted('ROLE_ROOT')) {
                throw new AccessDeniedException('You have no access to this page');
            }
        }

        $emailTemplate = $this->get('innomedio.backend_theme.object_language_helper')->updateLanguages($emailTemplate);

        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.email.email_templates', $this->generateUrl('innomedio.email.templates.list'));
        $this->header()->addBreadcrumb('innomedio.email.email_template.' . $action);

        $form = $this->createForm(EmailTemplateType::class, $emailTemplate, array('is_root' => $this->isGranted('ROLE_ROOT')));

        return $this->render('@InnomedioEmail/backend/templates/form.html.twig', array(
            'form' => $form->createView(),
            'info' => $emailTemplate->getInfo(),
            'activeNav' => 'admin',
            'activeSubnav' => 'email_templates',
        ));
    }

    /**
     * @Route(
     *     "/submit/{id}",
     *     name="innomedio.email.templates.submit"),
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     *
     * @param EmailTemplate|null $emailTemplate
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function submit(EmailTemplate $emailTemplate = null, Request $request)
    {
        $this->check();

        $em = $this->getDoctrine()->getManager();
        $response = new AjaxResponse();

        if (!$emailTemplate) {
            $emailTemplate = new EmailTemplate();
        }

        $form = $this->createForm(EmailTemplateType::class, $emailTemplate, array('is_root' => $this->isGranted('ROLE_ROOT')));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($emailTemplate);
            $em->flush();

            $response->setSuccess(true);
            $response->setRedirect($this->generateUrl('innomedio.email.templates.list'));
        } else {
            $response->setErrors($this->get('innomedio.backend_theme.error_parser')->getErrors($form, $emailTemplate));
        }

        return new JsonResponse($response->getResponse());
    }

    /**
     * @Route("/remove/{id}", requirements={"id" = "\d+"}, name="innomedio.email.templates.remove")
     * @param EmailTemplate $emailTemplate
     * @return JsonResponse
     */
    public function remove(EmailTemplate $emailTemplate)
    {
        $this->check(true);

        $em = $this->getDoctrine()->getManager();

        $em->remove($emailTemplate);
        $em->flush();

        return new JsonResponse(array(
            'success' => true
        ));
    }

    /**
     * @Security("has_role('ROLE_ROOT')")
     * @Route("/logs/{id}", requirements={"id" = "\d+"}, name="innomedio.email.templates.logs")
     * @param EmailTemplate $emailTemplate
     * @return Response
     */
    public function logs(EmailTemplate $emailTemplate)
    {
        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.email.email_templates', $this->generateUrl('innomedio.email.templates.list'));
        $this->header()->addBreadcrumb('innomedio.email.email_template.edit', $this->generateUrl('innomedio.email.templates.form', array('id' => $emailTemplate->getId())));
        $this->header()->addBreadcrumb('innomedio.backend_theme.global.logs');

        $logObjects = array($emailTemplate, $emailTemplate->getTranslations());
        $logs = $this->get('innomedio.backend_theme.loggable_helper')->getLogs($logObjects);

        return $this->render('@InnomedioBackendTheme/logs/list.html.twig', array(
            'logs' => $logs,
            'activeNav' => 'admin',
            'activeSubnav' => 'email_templates',
        ));
    }

    /**
     *
     */
    private function check($rootOnly = false)
    {
        if (!$this->getParameter('innomedio_email.has_templates') || ($rootOnly === true && !$this->isGranted('ROLE_ROOT'))) {
            throw new AccessDeniedException('You are not allowed here');
        }
    }
}