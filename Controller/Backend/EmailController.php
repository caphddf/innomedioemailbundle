<?php
namespace Innomedio\EmailBundle\Controller\Backend;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\EmailBundle\Entity\Mail;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("has_role('ROLE_EMAIL')")
 * @Route("/admin/emails")
 */
class EmailController extends BackendThemeController
{
    /**
     * @Route("/", name="innomedio.email.index")
     * @return Response
     */
    public function list()
    {
        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.email.emails');

        return $this->render('@InnomedioEmail/backend/emails/list.html.twig', array(
            'mails' => $this->getDoctrine()->getRepository('InnomedioEmailBundle:Mail')->findBy(array(), array('sent' => 'desc'))
        ));
    }

    /**
     * @Route("/view/{id}", requirements={"id" = "\d+"}, name="innomedio.email.view")
     *
     * @param Mail $mail
     * @return Response
     */
    public function view(Mail $mail)
    {
        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.email.emails', $this->generateUrl('innomedio.email.index'));
        $this->header()->addBreadcrumb('innomedio.email.view_title');

        return $this->render('@InnomedioEmail/backend/emails/view.html.twig', array(
            'mail' => $mail,
            'attachments' => json_decode($mail->getAttachments()),
            'activeNav' => 'admin',
            'activeSubnav' => 'email',
            'log' => json_decode($mail->getPostmarkInfo())
        ));
    }

    /**
     * @Route("/email/{id}", requirements={"id" = "\d+"}, name="innomedio.email.html")
     *
     * @param Mail $mail
     *
     * @return Response
     */
    public function showEmail(Mail $mail)
    {
        return $this->render('@InnomedioEmail/backend/emails/email.html.twig', array(
            'html' => $mail->getHtml()
        ));
    }

    /**
     * @Route("/remove/{id}", requirements={"id" = "\d+"}, name="innomedio.email.remove")
     * @Security("has_role('ROLE_ROOT')")
     * @param Mail $mail
     * @return JsonResponse
     */
    public function ajaxRemove(Mail $mail)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($mail);
        $em->flush();

        return new JsonResponse(array(
            'success' => true
        ));
    }

    /**
     * @Route("/download/{location}", name="innomedio.email.download")
     * @param $location
     */
    public function downloadAttachment($location)
    {
        return $this->file(urldecode($location));
    }
}