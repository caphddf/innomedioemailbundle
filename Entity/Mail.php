<?php
namespace Innomedio\EmailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Mail
 * @ORM\Table(name="mail")
 * @ORM\Entity(repositoryClass="Innomedio\EmailBundle\Repository\MailRepository")
 * @package App\Entity
 */
class Mail
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string")
     */
    private $tag;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string")
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="send_to", type="string")
     */
    private $to;

    /**
     * @var string
     *
     * @ORM\Column(name="headers", type="text")
     */
    private $headers;

    /**
     * @var string
     *
     * @ORM\Column(name="html", type="text")
     */
    private $html;

    /**
     * @var string|null
     *
     * @ORM\Column(name="errors", type="text", nullable=true)
     */
    private $errors;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $sent;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $postmarkId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $postmarkInfo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $attachments;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     */
    public function setTag(string $tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $title
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo(string $to): void
    {
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getHeaders(): string
    {
        return $this->headers;
    }

    /**
     * @param string $headers
     */
    public function setHeaders(string $headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @return string
     */
    public function getHtml(): string
    {
        return $this->html;
    }

    /**
     * @param string $html
     */
    public function setHtml(string $html): void
    {
        $this->html = $html;
    }

    /**
     * @return string|null
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param string|null $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return \DateTime
     */
    public function getSent(): \DateTime
    {
        return $this->sent;
    }

    /**
     * @param \DateTime $sent
     */
    public function setSent(\DateTime $sent): void
    {
        $this->sent = $sent;
    }

    /**
     * @return mixed
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param mixed $attachments
     */
    public function setAttachments($attachments): void
    {
        $this->attachments = $attachments;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPostmarkId()
    {
        return $this->postmarkId;
    }

    /**
     * @param mixed $postmarkId
     */
    public function setPostmarkId($postmarkId): void
    {
        $this->postmarkId = $postmarkId;
    }

    /**
     * @return mixed
     */
    public function getPostmarkInfo()
    {
        return $this->postmarkInfo;
    }

    /**
     * @param mixed $postmarkInfo
     */
    public function setPostmarkInfo($postmarkInfo): void
    {
        $this->postmarkInfo = $postmarkInfo;
    }
}