<?php

namespace Innomedio\EmailBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Gedmo\Loggable
 * @ORM\Entity(repositoryClass="Innomedio\EmailBundle\Repository\EmailTemplateRepository")
 */
class EmailTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection|EmailTemplateTranslation[]
     *
     * @ORM\OneToMany(targetEntity="Innomedio\EmailBundle\Entity\EmailTemplateTranslation", mappedBy="emailTemplate", cascade={"persist", "remove"}, indexBy="language_id")
     */
    private $translations;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $tag;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    private $info;

    /**
     * Translation constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param EmailTemplateTranslation $translation
     */
    public function addTranslation(EmailTemplateTranslation $translation)
    {
        $translation->setEmailTemplate($this);

        if (!$this->translations->contains($translation)) {
            $this->translations->add($translation);
        }
    }

    /**
     * @param EmailTemplateTranslation $translation
     */
    public function removeTranslation(EmailTemplateTranslation $translation)
    {
        $this->translations->remove($translation);
    }

    /**
     * @return ArrayCollection|EmailTemplateTranslation[]
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info): void
    {
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }
}