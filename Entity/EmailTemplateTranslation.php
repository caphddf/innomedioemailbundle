<?php
namespace Innomedio\EmailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Innomedio\BackendThemeBundle\Entity\Language;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Gedmo\Loggable
 * @ORM\Table(name="email_template_translation", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="email_template_language_translation", columns={"language_id", "email_template_id"})
 * })
 * @ORM\Entity(repositoryClass="Innomedio\EmailBundle\Repository\EmailTemplateTranslationRepository")
 */
class EmailTemplateTranslation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Language|null
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\BackendThemeBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $language;

    /**
     * @var EmailTemplate|null
     * @ORM\ManyToOne(targetEntity="Innomedio\EmailBundle\Entity\EmailTemplate", inversedBy="translations")
     * @ORM\JoinColumn(name="email_template_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $emailTemplate;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @return Language|null
     */
    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    /**
     * @param Language|null $language
     */
    public function setLanguage(?Language $language): void
    {
        $this->language = $language;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return EmailTemplate|null
     */
    public function getEmailTemplate(): ?EmailTemplate
    {
        return $this->emailTemplate;
    }

    /**
     * @param EmailTemplate|null $emailTemplate
     */
    public function setEmailTemplate(?EmailTemplate $emailTemplate): void
    {
        $this->emailTemplate = $emailTemplate;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }
}